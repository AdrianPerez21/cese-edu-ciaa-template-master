#include "sapi.h"               // <= sAPI header

// MPU9250 Address
MPU9250_address_t addr = MPU9250_ADDRESS_0; // If MPU9250 AD0 pin is connected to GND

/* FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE RESET. */
int main(void){
   /* ------------- INICIALIZACIONES ------------- */
   boardConfig();
   // Inicializar la IMU
   printf("Inicializando IMU MPU9250...\r\n" );
   int8_t status;
   status = mpu9250Init( addr );

   if( status < 0 ){
      printf( "IMU MPU9250 no inicializado, chequee las conexiones:\r\n\r\n" );
      printf( "MPU9250 ---- EDU-CIAA-NXP\r\n\r\n" );
      printf( "    VCC ---- 3.3V\r\n" );
      printf( "    GND ---- GND\r\n" );
      printf( "    SCL ---- SCL\r\n" );
      printf( "    SDA ---- SDA\r\n" );
      printf( "    AD0 ---- GND\r\n\r\n" );
      printf( "Se detiene el programa.\r\n" );
      while(1);
   }
   printf("IMU MPU9250 inicializado correctamente.\r\n\r\n" );

   /* ------------- REPETIR POR SIEMPRE ------------- */
   while(TRUE){
      //Leer el sensor y guardar en estructura de control
      mpu9250Read();
      // printf("Controlando si se leyo bien: [0 mal] [1 bien] . %i \r\n\r\n", mpu9250Read());

      // Imprimir resultados
      printf( "Giroscopo:      (%f, %f, %f)   [rad/s]\r\n",
              mpu9250GetGyroX_rads(),
              mpu9250GetGyroY_rads(),
              mpu9250GetGyroZ_rads()
            );

		printf( "Acelerometro:   (%f, %f, %f)   [m/s2]\r\n",
              mpu9250GetAccelX_mss(),
              mpu9250GetAccelY_mss(),
              mpu9250GetAccelZ_mss()
            );

		printf( "Magnetometro:   (%f, %f, %f)   [uT]\r\n",
              mpu9250GetMagX_uT(),
              mpu9250GetMagY_uT(),
              mpu9250GetMagZ_uT()
            );

		printf( "Temperatura:    %f   [C]\r\n\r\n",
              mpu9250GetTemperature_C()
            );

      delay(1000);
   }

   /* NO DEBE LLEGAR NUNCA AQUI, debido a que a este programa no es llamado
      por ningun S.O. */
   return 0 ;
}

/*==================[end of file]============================================*/
