#include "sapi.h" // <= sAPI header
#include <string.h>
#include <stdio.h> // para convertir float a string con la funcion gcvt()
#define MAX_LONG_STRING 10 //longitud del string que representará a las datos float de la lectura del acelerometro,giroscopio,etc

MPU9250_address_t addr = MPU9250_ADDRESS_0; // If MPU9250 AD0 pin is connected to GND

/*==================[definiciones y macros]==================================*/

#define UART_PC UART_USB
#define UART_BLUETOOTH UART_232

bool_t hm10bleTest(int32_t uart);
void hm10blePrintATCommands(int32_t uart);
//funciones auxiliares para convertir float a string 
void reverse(char *str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char *res, int afterpoint);

int main(void){
   // Inicializar y configurar la plataforma
   boardConfig(); 

   uartConfig(UART_PC, 9600); // Inicializar UART_USB para conectar a la PC
   uartWriteString(UART_PC, "\r\n\r\n\r\n\r\n >> UART_PC configurada. \r\n");

   printf("\r\n==== INICIALIZANDO Y CONFIGURANDO LA PLATAFORMA =====\n\r");

   /* INICIALIZO LA IMU */

   printf("\r\nInicializando IMU MPU9250...\r\n");
   int8_t status;
   status = mpu9250Init(addr);
   if (status < 0)   {
      printf("IMU MPU9250 no inicializado, chequee las conexiones:\r\n\r\n");
      printf("MPU9250 ---- EDU-CIAA-NXP\r\n\r\n");
      printf("    VCC ---- 3.3V\r\n");
      printf("    GND ---- GND\r\n");
      printf("    SCL ---- SCL\r\n");
      printf("    SDA ---- SDA\r\n");
      printf("    AD0 ---- GND\r\n\r\n");
      printf("Se detiene el programa.\r\n");
      while (1)
         ;
   }
   // NOTA: status = 1 significa que la inicializacion finalizo satisfactoriamente.
   printf("IMU MPU9250 inicializado correctamente.\r\n\r\n");



   /* INICIALIZO LA CONEXION UART_232 de la placa EDUCIAA para conectarse al modulo bluetooth*/

   uartConfig(UART_BLUETOOTH, 9600); // Inicializar UART_232 para conectar al modulo bluetooth
   uartWriteString(UART_PC, "UART_BLUETOOTH para modulo Bluetooth configurada.\r\n");

   uint8_t data = 0;

   uartWriteString(UART_PC, "Testeto si el modulo esta conectado enviando: AT\r\n");
   if (hm10bleTest(UART_BLUETOOTH))    {
      uartWriteString(UART_PC, "Modulo bluetooth conectado correctamente.\r\n");
   }
   else   {
      uartWriteString(UART_PC, "Modulo Bluetooth no funciona.\r\n");
   }



   // ---------- REPETIR POR SIEMPRE --------------------------
   while (TRUE)
   {
      // Si leo un dato de una UART lo envio al otra (bridge)
      if (uartReadByte(UART_PC, &data))
      {
         uartWriteByte(UART_BLUETOOTH, data);
      }
      if (uartReadByte(UART_BLUETOOTH, &data))
      {
         if (data == 'h')
         {
            gpioWrite(LEDB, ON);
         }
         if (data == 'l')
         {
            gpioWrite(LEDB, OFF);
         }
         uartWriteByte(UART_PC, data);
      }

      // Si presiono TEC1 imprime(imprime en la terminal serial) la lista de comandos AT que puede recibir el modulo bluetooth (precondifcion: ningun celular debe estar conectado al modulo bluettoht)
      if (!gpioRead(TEC1))      {
         hm10blePrintATCommands(UART_BLUETOOTH);
      }
      // Si presiono TEC2 leo los datos sensor acelerometro y los transfiero al celular mediante el modulo bluetooth,
      if (!gpioRead(TEC2))      {
         
         //Leer los datos y guardarlos en una estructura de control
         mpu9250Read();
         // printf("Controlo si se leyo bien o mal: [0=mal] [1=bien] . %i \r\n\r\n", mpu9250Read());

         // NOTA: para mostrar los resultados en el serial-terminal(putty) unicamente es posible usando printf("%s",myString).  Es decir convirtiendo los FLOAT en STRING (por algun motivo no se puede mostrar float en este codigo. En el codigo del acelerometro mpu9250.c si se puede)
         
         char myStringTemporal1[MAX_LONG_STRING], myStringTemporal2[MAX_LONG_STRING], myStringTemporal3[MAX_LONG_STRING]; // variables temporales para transferir string al dispositivo bluetooth con la funcion uartWriteString();

         /*    GIROSCOPIO   */
         // uso la funcion gctv() para convertir las lectura  en string
         gcvt(mpu9250GetGyroX_rads(), 3, myStringTemporal1);
         gcvt(mpu9250GetGyroY_rads(), 3, myStringTemporal2);
         gcvt(mpu9250GetGyroZ_rads(), 3, myStringTemporal3);

         // Muestro lecturas en el serialTerminal (putty)
         printf("Giroscopo:      (%s, %s, %s)   [rad/s]\r\n",
                myStringTemporal1,
                myStringTemporal2,
                myStringTemporal3);

         // Enviar lecturas (string) al dispositivo bluetooth
         //concateno todos los datos en una sola variable string temp
         char temp[500] = "Giroscopio:            ("; // declaro un string con longitud de 500 caracteres
         strcat(temp, myStringTemporal1);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal2);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal3);
         strcat(temp, ")   [rad/s]\n");
         uartWriteString(UART_BLUETOOTH, temp); // envio la cadena temp al celular


         /*    ACELEROMETRO   */
         // uso la funcion gctv() para convertir las lectura  en string
         gcvt(mpu9250GetAccelX_mss(), 4, myStringTemporal1);
         gcvt(mpu9250GetAccelY_mss(), 4, myStringTemporal2);
         gcvt(mpu9250GetAccelZ_mss(), 4, myStringTemporal3);

         // Muestro lecturas en el serialTerminal (putty)
         printf("Acelerometro:      (%s, %s, %s)   [m/s2]\r\n",
                myStringTemporal1,
                myStringTemporal2,
                myStringTemporal3);

         // Enviar lecturas (string) al dispositivo bluetooth
         strcpy(temp, ""); // limpiamos la cadena 
         strcat(temp, "Acelerometro:         (");
         strcat(temp, myStringTemporal1);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal2);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal3);
         strcat(temp, ")   [m/s2]\n");
         uartWriteString(UART_BLUETOOTH, temp);


         /*    MAGNOMETRO   */
         // uso la funcion gctv() para convertir las lectura  en string
         gcvt(mpu9250GetMagX_uT(), 4, myStringTemporal1);
         gcvt(mpu9250GetMagY_uT(), 4, myStringTemporal2);
         gcvt(mpu9250GetMagZ_uT(), 4, myStringTemporal3);

         // Muestro lecturas en el serialTerminal (putty)
         printf("Magnometro:      (%s, %s, %s)   [uT]\r\n",
                myStringTemporal1,
                myStringTemporal2,
                myStringTemporal3);
         
         // Enviar lecturas (string) al dispositivo bluetooth
         strcpy(temp, ""); // limpiamos la cadena
         strcat(temp, "Magnometro:         (");
         strcat(temp, myStringTemporal1);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal2);
         strcat(temp, ", ");
         strcat(temp, myStringTemporal3);
         strcat(temp, ")   [uT]\n");
         uartWriteString(UART_BLUETOOTH, temp);


         /*    TEMPERATURA   */
         // uso la funcion gctv() para convertir las lectura  en string
         gcvt(mpu9250GetTemperature_C(), 4, myStringTemporal1);
         
         // Muestro lecturas en el serialTerminal (putty)
         printf("Temperatura:      (%s)   [°C]\r\n",
                myStringTemporal1);
         printf("-----------------\n\r");

         // Enviar lecturas (string) al dispositivo bluetooth
         strcpy(temp, ""); // limpiamos la cadena
         strcat(temp, "Temperatura:         (");
         strcat(temp, myStringTemporal1);
         strcat(temp, ")   [°C]\n");
         uartWriteString(UART_BLUETOOTH, temp);

         uartWriteString(UART_BLUETOOTH, "-----------------\n");
         delay(1000);
      }



      // Si presiono TEC3 enciende el led de la pantalla de la app
      if (!gpioRead(TEC3))
      {
         uartWriteString(UART_BLUETOOTH, "LED_ON\r\n");
         delay(500);
      }
      // Si presiono TEC4 apaga el led de la pantalla de la app
      if (!gpioRead(TEC4))
      {
         uartWriteString(UART_BLUETOOTH, "LED_OFF\r\n");
         delay(500);
      }
   }

   // NO DEBE LLEGAR NUNCA AQUI, debido a que a este programa se ejecuta
   // directamenteno sobre un microcontroladore y no es llamado por ningun
   // Sistema Operativo, como en el caso de un programa para PC.
   return 0;
}

/*==================[definiciones de funciones internas]=====================*/

/*==================[definiciones de funciones externas]=====================*/

bool_t hm10bleTest(int32_t uart)
{
   uartWriteString(uart, "AT\r\n");
   return waitForReceiveStringOrTimeoutBlocking(uart,
                                                "OK\r\n", strlen("OK\r\n"),
                                                1000);
}

void hm10blePrintATCommands(int32_t uart)
{
   delay(500);
   uartWriteString(uart, "AT+HELP\r\n");
}

/*==================[fin del archivo]========================================*/

