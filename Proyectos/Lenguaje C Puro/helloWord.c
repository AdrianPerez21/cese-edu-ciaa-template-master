// Para compilar el archivo helloWord.c con linkear la libreria libm.a de<math.h> poner : gcc -o myArchivoDeSalida helloWord.c -lm
// Para ejecutar poner: ./salida

/* Programa: Hola mundo */

// #include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
    // #include <stdio.h>  // para convertir float a string con la funcion ftoa()
    // #include <stdlib.h> // para convertir float a string con la funcion ftoa()??
    // #include <math.h>   // para convertir float a string con la funcion ftoa()??
    /*==================[declaraciones de funciones externas]====================*/

    //funciones auxiliares para convertir float a string
void reverse(char *str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char *res, int afterpoint);

// int main()
// {
//     // printf("Hola mundo.\n");

//     // char String[7];
//     char* myString;
//     float T = 1.2345;

//     int myEntero = sprintf(myString,"BUENOOO %f", T);

//     printf("Mi entero es: %i",myEntero);
//     // getch(); /* Pausa */

//     return 0;
// }

//para convertir float a string
// reverses a string 'str' of length 'len'
void reverse(char *str, int len)
{
    int i = 0, j = len - 1, temp;
    while (i < j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

// Converts a given integer x to string str[].  d is the number
// of digits required in output. If d is more than the number
// of digits in x, then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating point number to string.
void ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

// driver program to test above funtion
int main()
{
    char res[20];
    float n = 233.007;
    ftoa(n, res, 4);
    printf("\n\"%s\"\n", res);

    //este bloque de codigo compuesto por 5 lineas, sirve para entender el concepto de PUNTERO 
    printf("\n=======Entendiendo el concepto de PUNTEROS=============\n");
    int a = 1988;   //Declaración de variable entera de tipo entero  asignacion de mi año de nacimiento
    int *puntero; //Declaración de variable puntero de tipo entero
    puntero = &a; //Asignación de la dirección memoria de a
    printf("El valor de a es: %d. \n El dato guardado en la direccion del *puntero es: %d. \n", a, *puntero);
    printf("La dirección de memoria a la que apunta el *puntero es: %p \n", puntero);
    printf("=======================================================\n");

    
    return 0;
}
